#Problem 1 Database
#Author: Anu Dhawan
#Date Created: 20-04-2015

DROP DATABASE IF EXISTS prob1;

CREATE DATABASE prob1;

USE prob1;

DROP TABLE IF EXISTS user_data;
CREATE TABLE user_data(
	username VARCHAR(20) PRIMARY KEY,
	status BOOLEAN,
	password VARCHAR(20)
);



INSERT INTO user_data VALUES ('Tony', 1, 'Tony123');
INSERT INTO user_data VALUES ('Mary', 1, 'Mary123');
INSERT INTO user_data VALUES ('Alex', 0, 'Alex123');
INSERT INTO user_data VALUES ('Marina', 1, 'Marina123');
INSERT INTO user_data VALUES ('Colin', 1, 'Colin123');
INSERT INTO user_data VALUES ('Rudy', 0, 'Rudy123');
INSERT INTO user_data VALUES ('Miko', 1, 'Miko123');
